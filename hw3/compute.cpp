
#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <cstdlib>

void apply_phi(int permutation, 
               int number_of_edges,
               std::vector< std::pair<int,int>* >* expansion);

void map_vertex_degree_pair(int bits, std::pair<int,int>* degree, int index); 

bool compare_expansions(std::vector< std::pair<int,int>* >* left,
                        std::vector< std::pair<int,int>* >* right);

void free_pairs(std::vector< std::pair<int,int>* >* collection);

void print_header();

//debug func
void print_expansion(std::vector< std::pair<int,int>* >* expansion);


/*
   argument #edges limited to 31
   modify operation to bitstream to increase domain
*/
int main(int argc, char* argv[])
{

    std::vector< std::vector< std::pair<int,int>* >* > isomorphisms;
    std::vector< std::vector< std::pair<int,int>* >* > expansions;

    int number_of_edges = 0;

    //argv
    int number_of_edges_max = 0;
    if(argc > 1)
        number_of_edges_max = atoi(argv[1]);
    
    print_header();
    
while(number_of_edges < number_of_edges_max+1)
{
 
    //for each permutation
    for(int permutation = 0;
        permutation < pow(2,number_of_edges);
        permutation++)
    {
        std::vector< std::pair<int,int>* >* expansion = new(std::vector< std::pair<int,int> *>);
        //apply phi to permutation
        apply_phi(permutation,number_of_edges,expansion);
        expansions.push_back(expansion);
    }


    std::vector< std::vector < std::pair<int,int>*>*>::iterator index;
    int index2 = 0;

    //for each expansion in expansions
    while(!expansions.empty())
    {
        index = expansions.begin();
        //append expansion to isomorphisms
        isomorphisms.push_back((*index));

        //reverse
        std::vector< std::pair<int,int>*>* reversed 
                = new(std::vector< std::pair<int,int>*>);

        while(!((*index))->empty())
        {
            //loss of isomorphism information
            reversed->push_back((*index)->back());
            (*index)->pop_back();
        }

        expansions.erase(index);

        //compare to each expansion in expansions
        index2 = 0;
        while(index2 < expansions.size())
        {
            //match is isomorphic
            if(compare_expansions(reversed,expansions.at(index2)))
            {
                //remove match from expansions
                free_pairs(*(expansions.begin()+index2));
                free(*(expansions.begin()+index2));
                expansions.erase(expansions.begin()+index2);
                index2--;
            }
        index2++;
        }
    free_pairs(reversed);
    free(reversed);
    }


    std::cout<<number_of_edges<<"\t"
             <<isomorphisms.size()<<"\t"
             <<(pow(2,number_of_edges) - isomorphisms.size())<<"\t"
             <<isomorphisms.size()-(pow(2,number_of_edges) - isomorphisms.size())<<"\t"
             <<std::endl;

    while(!isomorphisms.empty())
    {
        free_pairs(isomorphisms.back());
        isomorphisms.pop_back();
    }
    isomorphisms.clear();
    number_of_edges++;
}//end main loop
}

void apply_phi(int permutation, 
               int number_of_edges,
               std::vector< std::pair<int,int>* >* expansion)
{
    int mask = 3;//11 
    int index = 0;
    std::pair<int,int>* new_pair;

    for(index = number_of_edges;
        index > 0;
        index--) 
    {
        new_pair = new(std::pair<int,int>);

        map_vertex_degree_pair(
        ((permutation & (mask << (index-1)))>>(index-1)),
        new_pair,
        (number_of_edges - index)
        );
        expansion->push_back(new_pair);
    }
    //terminal vertex
    new_pair = new(std::pair<int,int>);
    map_vertex_degree_pair(
    (permutation & (mask>>1)),//either in 0 or out 0
    new_pair,
    -1
    );
    expansion->push_back(new_pair);
}
//position signifys relative position in path
//  given a 'left-right' orientation 
//      if position 0 then v_0 vertex
//      -1 signify v_k vertex

void map_vertex_degree_pair(int bits, std::pair<int,int>* degree, int position) 
{
    //first = in
    //second = out
    switch(bits)
    {
        case 0:
            if(position == 0)
            {
                degree->first = 0;
                degree->second = 1;
            }
            else if(position > 0)
            {
                degree->first = 1;
                degree->second = 1;
            }
            else
            {
                degree->first = 1;
                degree->second = 0;
            }
            break;
        case 1:
            if(position == 0)
            {
                degree->first = 1;
                degree->second = 0;
            }
            else if(position > 0)
            {
                degree->first = 2;
                degree->second = 0;
            }
            else
            {
                degree->first = 0;
                degree->second = 1;
            }
            break;
        case 2:
            degree->first = 0;
            degree->second = 2;
            break;
        case 3:
            degree->first = 1;
            degree->second = 1;
            break;
        default:
            std::cerr<<"map domain error"<<std::endl;
    }
}
//true if match
bool compare_expansions(std::vector< std::pair<int,int>* >* left,
                        std::vector< std::pair<int,int>* >* right)
{
    bool equal = true;
    //for at index firsta=firstb && seconda=secondb
    for(int index = 0;
        index < left->size();
        index++)
    {
        if( (left->at(index)->first != right->at(index)->first)
         || (left->at(index)->second != right->at(index)->second))
        {
            equal = false;
            break;
        }
    }
    return equal;
}

//debug
void print_expansion(std::vector< std::pair<int,int>* >* expansion)
{
    for(int index = 0;
        index < expansion->size();
        index++)
    {
        std::cout<<"In degree: "<<expansion->at(index)->first
                 <<" Out degree: "<<expansion->at(index)->second
                 <<std::endl;
    }
}

void free_pairs(std::vector< std::pair<int,int>* >* collection)
{
    while(!collection->empty())
    {
        free(collection->back());
        collection->pop_back();
    }
}

void print_header()
{

    std::cout<<"E: \t"
             <<"O: \t"
             <<"P-O: \t"
             <<"O-(P-O) "
             <<std::endl;
}



