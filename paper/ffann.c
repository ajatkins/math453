
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

struct edge
{
};

struct node
{
    double forward_activation_value;//g(x)
    double delta;//delta value for backpropagation algorithm
    /*
        in-neighborhood cooresponds to the previous layer 
        array of double weight values, index is node of layer_i-1
        weights[0] cooresponds to bias
    */
    double *inneighborhood;//allocation on heap; generalized 
};

struct layer
{
    double bias_value;
    int number_of_nodes;
    struct node **layer;
    //list node (includes bias implicitly since bias represented in inneighborhood)
};

struct ffann
{
    int number_of_layers;
    double alpha;//learning rate
    /*
        layer[0] is the inputlayer
        layer[0<i<number_of_layers-1] are hidden layers
        layer[number_of_layers-1] is the output layer
    */
    struct layer **layers;
};
/*
    parameters example
        , int count, int array[]);
        count = number of layers, array[i] = number of node at layer i
*/
//void initialize_network(struct ffann** nn, /*parameters*/); 
void initialize_network(struct ffann **nn, 
                        int number_of_layers,
                        int nodes_per_layer[],
                        double learning_rate);
void forward_propagation(struct ffann **nn, double *input_vector);
double forward_propagation_evaluation(struct ffann **nn, double *input_vector);
void backpropagation(struct ffann **nn, double *expected_vector);
void update_edge_weights(struct ffann **nn, int j_node, int layer);
void construct_activation_vector(struct ffann **nn, double **vector, int layer);
double logistic_function_derivative(double node_dot_product);
double logistic_function(double node_dot_product);
double node_dot_product(struct node *node, double *input_vector, int vector_size);
double activation_function(/*sum weights*/);
double sum_edge_delta(struct ffann **nn, int i_node, int layer);
void connect_layer(/*layer1, layer2, connections by index*/);
//layer_i-1 fully connected to layer_i
void partial_complete_connect(struct ffann **nn, int range);
void print_edge_weights(struct ffann **nn);
double generate_halfinterval_double(int range);
void generate_input_vector_XOR(double **vector, int size);
double compute_XOR(double x_1, double x_2);
void free_ffann(struct ffann **nn);
void free_sample_set(double **set, int size);
int main(void)
{
    srand(time(NULL));

    //structure formation test
    struct ffann *nn = malloc(sizeof(struct ffann));
    int node_at_layer[] = { 2, 2, 1 };
    int number_of_layers = 3;
    double learning_rate = 0.85;//fixed; could be decreasing to simiulate annealing
    initialize_network(&nn,number_of_layers,node_at_layer,learning_rate);

/*
    //random number generator test
    srand(time(NULL));
    int count = 0;
    while(count < 5)
    {
        fprintf(stdout,"\n %f", generate_halfinterval_double(5));
        count++;
    }
    fprintf(stdout,"\n");
*/
    //test connect
    partial_complete_connect(&nn,10);
//    print_edge_weights(&nn);
    int count = 0;
    double error_sum = 0;
do{
    error_sum = 0;
    //generate sample set
    double **sample_set;
    int sample = 0;
    int number_of_samples = 1000000;
    sample_set = malloc(sizeof(double*)*number_of_samples);
    generate_input_vector_XOR(sample_set,number_of_samples);

    for(sample = 0;
        sample < number_of_samples;
        sample++)
    {
        forward_propagation(&nn, sample_set[sample]);
        backpropagation(&nn,sample_set[sample]);
    }
    free_sample_set(sample_set, number_of_samples);

    double **evaluation_set;
    number_of_samples = 100;
    evaluation_set = malloc(sizeof(double*)*number_of_samples);
    generate_input_vector_XOR(evaluation_set,number_of_samples);
    for(sample = 0;
        sample < number_of_samples;
        sample++)
    {
        error_sum += forward_propagation_evaluation(&nn, evaluation_set[sample]);
    }
    error_sum /= number_of_samples;//avg error
    free_sample_set(evaluation_set, number_of_samples);
    count++;
    fprintf(stdout,"Aerror: %f Count: %d\n", error_sum, count);

    //rerandomize edges (in local minimum)
    if( (error_sum > .1) && (count > 50))
    {
        free_ffann(&nn);
        nn = malloc(sizeof(struct ffann));
        initialize_network(&nn,number_of_layers,node_at_layer,learning_rate);
        partial_complete_connect(&nn,10);
        count = 0;
    }

}while( (error_sum > 0.005) && (count < 100000));

    print_edge_weights(&nn);
    free_ffann(&nn);

exit(0);
}

void initialize_network(struct ffann** nn, 
                        int number_of_layers,
                        int nodes_per_layer[],
                        double learning_rate)
{
    if((*nn)==NULL)
        exit(1);

    (*nn)->alpha = learning_rate;
    (*nn)->number_of_layers = number_of_layers;

    (*nn)->layers = malloc(sizeof(struct layer*)*number_of_layers);

    if((*nn)->layers == NULL)
    {
        fprintf(stderr,"ffann::init malloc error\n");
        exit(1);
    }

    for( int index = 0;
        index < number_of_layers;
        index++)
    {
        (*nn)->layers[index] = malloc(sizeof(struct layer));

        if((*nn)->layers[index] == NULL)
        {
            fprintf(stderr,"ffann::layer malloc error\n");
            exit(1);
        }
        (*nn)->layers[index]->bias_value = 1;
        (*nn)->layers[index]->number_of_nodes = nodes_per_layer[index];
        (*nn)->layers[index]->layer = 
            malloc(sizeof(struct node*)*(*nn)->layers[index]->number_of_nodes);

        for( int index2 = 0;
            index2 < nodes_per_layer[index];
            index2++)
        {
           (*nn)->layers[index]->layer[index2] = malloc(sizeof(struct node));

            if((*nn)->layers[index] == NULL)
            {
                fprintf(stderr,"ffann::node malloc error\n");
                exit(1);
            }
            //following values to be set in void connect_layer();
            (*nn)->layers[index]->layer[index2]->forward_activation_value = 0;
            (*nn)->layers[index]->layer[index2]->inneighborhood = NULL;
        }
    }
}
//connects node at layer_i to every node at layer_i-1
    //assigns random value -5.0 < x < 5.0 to edge weight with .5 intervals
void partial_complete_connect(struct ffann** nn, int range)
{
    for( int layer = 1; 
        layer < (*nn)->number_of_layers;
        layer++)
    {
        for( int node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            (*nn)->layers[layer]->layer[node]->inneighborhood =
                malloc(sizeof(double)*(((*nn)->layers[layer-1]->number_of_nodes)+1));

            //intialize bias
            (*nn)->layers[layer]->layer[node]->inneighborhood[0] =
                (*nn)->layers[layer]->bias_value;

            for( int edge = 1;
                edge <= (*nn)->layers[layer-1]->number_of_nodes;
                edge++)
            {
                (*nn)->layers[layer]->layer[node]->inneighborhood[edge] =
                    generate_halfinterval_double(range);
            }
        }
    }
}

void print_edge_weights(struct ffann** nn)
{
    for( int layer = 1; 
        layer < (*nn)->number_of_layers;
        layer++)
    {
        fprintf(stdout, "\nLayer:%d\n", layer);
        for( int node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            for( int edge = 0;
                edge <= (*nn)->layers[layer-1]->number_of_nodes;
                edge++)
            {
                fprintf(stdout, "Edge: (%d,%d) Weight: %f\n",
                        edge, node,
                (*nn)->layers[layer]->layer[node]->inneighborhood[edge]);
            }
        }
    }
}
/*
    Given an input vector;
        Computes logistic function for each node at each layer

    Clear flaw in design with the conditional at dot_product
*/
void forward_propagation(struct ffann **nn, double *input_vector)
{
    //Check input vector for correct dimensions(assume so)
    //for each layer beyond input layer;
        //for each node
            //compute logistic function
    double *activation_vector;
    int node = 0;
    activation_vector = input_vector;
    //set activation values for input layer
    for(node = 0;
        node < (*nn)->layers[0]->number_of_nodes;
        node++)
    {
        (*nn)->layers[0]->layer[node]->forward_activation_value = 
            input_vector[node];
    }

    for( int layer = 1; 
        layer < (*nn)->number_of_layers;
        layer++)
    {
        for(node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            (*nn)->layers[layer]->layer[node]->forward_activation_value =
                logistic_function( node_dot_product(
                    (*nn)->layers[layer]->layer[node],
                    activation_vector,
                    (*nn)->layers[layer-1]->number_of_nodes));
        }
        if(layer > 1)
            free(activation_vector);
        if(layer+1 < (*nn)->number_of_layers)
            construct_activation_vector(nn, &activation_vector, layer);
    }
}
//specialized to the XOR problem training set; consider vector input
    //expected vector contains a single value, final entry of vector
void backpropagation(struct ffann **nn, double *expected_vector)
{
    double *activation_vector;
    int output_layer = (*nn)->number_of_layers-1;
    construct_activation_vector(nn,&activation_vector,output_layer-1);
 
    int node = 0;
    int layer = 0;
    //observed error of output layer
    for(node = 0;
        node < (*nn)->layers[output_layer]->number_of_nodes;
        node++)
    {
        //delta of output layer;woah ugly
        (*nn)->layers[output_layer]->layer[node]->delta =
            ( (logistic_function_derivative( node_dot_product(
                (*nn)->layers[output_layer]->layer[node],
                activation_vector,
                (*nn)->layers[output_layer-1]->number_of_nodes)))
                * (expected_vector[2] - 
                  (*nn)->layers[output_layer]->layer[node]->forward_activation_value));
    }
    free(activation_vector);

    //for each layer until the input layer
    for(layer = output_layer-1;
        layer > 0;
        layer--)
    {
        construct_activation_vector(nn,&activation_vector,layer);
        //propagate detas
        //for each node at layer; delta[node]
        for(node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            (*nn)->layers[layer]->layer[node]->delta =
                ( (logistic_function_derivative( node_dot_product(
                    (*nn)->layers[layer]->layer[node],
                    activation_vector,
                    (*nn)->layers[layer-1]->number_of_nodes)))
                    * sum_edge_delta(nn,node,layer));
        }
        free(activation_vector);
    }
    //for each edge
    for(layer = output_layer;
        layer > 0;
        layer--)
    {
        for(node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            update_edge_weights(nn, node, layer);
        }
    }

}

void update_edge_weights(struct ffann **nn, int j_node, int layer)
{
    for(int i_node = 0;
        i_node < (*nn)->layers[layer-1]->number_of_nodes;
        i_node++)
    {
        //w_i,j = w_i,j + lr * a_i * delta[j]
        (*nn)->layers[layer]->layer[j_node]->inneighborhood[i_node+1] =
            (*nn)->layers[layer]->layer[j_node]->inneighborhood[i_node+1] 
            + ((*nn)->alpha
            * (*nn)->layers[layer-1]->layer[i_node]->forward_activation_value
            * (*nn)->layers[layer]->layer[j_node]->delta);
    }
}

//a bit of heuristic abuse; taking advantage of structure to reduce computation
    //notice inneighborhood has offset of 1 since [0] is bias
double sum_edge_delta(struct ffann **nn, int i_node, int layer)
{
    double sum = 0;
    //for each j_node at layer+1
    for(int j_node = 0;
        j_node < (*nn)->layers[layer+1]->number_of_nodes;
        j_node++)
    {
        //sum w_i,j * delta[j]
        sum += ((*nn)->layers[layer+1]->layer[j_node]->inneighborhood[i_node+1]
                * (*nn)->layers[layer+1]->layer[j_node]->delta);
    }
    return sum;
}

void construct_activation_vector(struct ffann **nn, double **vector, int layer)
{
    double *new_vector = 
        malloc(sizeof(double)*(*nn)->layers[layer]->number_of_nodes);

    if(new_vector == NULL)
    {
        fprintf(stderr,"malloc error, construct_vector\n");
        exit(1);
    }
    for( int node = 0;
        node < (*nn)->layers[layer]->number_of_nodes;
        node++)
    {
        new_vector[node] = 
            (*nn)->layers[layer]->layer[node]->forward_activation_value;
    }
    
    *vector = new_vector;
}

double logistic_function(double node_dot_product)
{
    return (1/(1+exp(-1*(node_dot_product))));
}

double logistic_function_derivative(double node_dot_product)
{
    return exp(-1*node_dot_product)/pow((1+exp(-1*(node_dot_product))),2);
}

double node_dot_product(struct node *node, double *input_vector, int vector_size)
{
    //bias
    double product = node->inneighborhood[0];

    for(int index = 1;
        index < vector_size+1;
        index++)
    {
        product += (node->inneighborhood[index] * input_vector[index-1]);
    }
}

double generate_halfinterval_double(int range)
{
    if(range > 1000)
    {
        fprintf(stderr,"generate_halfinterval_double::invalid range specified\n"); 
    }
    //right shift
    range = 2*range;

    double value = (double)(rand() % range + 1);

    if( value <= ((double)range / 2) )
        value *= -1;

    return (value/2);
}
//creates a pseudo-random sample set of binary XOR inputs
    //[0-1] hold inputs, [2] holds expected XOR output
    //we've specialized the forward/backward algroithms off this set
    //technically should be a set of {{input vector},{output vector}}
        //instead of {input1,input2,inputn,output1,ouputn}
void generate_input_vector_XOR(double **vector, int size)
{
    for( int index = 0;
        index < size;
        index++)
    {
        vector[index] = malloc(sizeof(double)*3);
        for( int index2 = 0;
            index2 < 2;
            index2++)
        {
            if(generate_halfinterval_double(500) < 0)
            {
                vector[index][index2] = 0;
            }
            else
            {
                vector[index][index2] = 1;
            }
        }
        vector[index][2] = compute_XOR(vector[index][0],vector[index][1]);
    }
}
/* print statement to verify output of compute_XOR in generate_sampleset
        fprintf(stdout, "Index: %d X_1: %d X_2: %d R: %d\n",
                        index, 
                        (int)vector[index][0],
                        (int)vector[index][1],
                        (int)vector[index][2]);
*/
double compute_XOR(double x_1, double x_2)
{
    return (double)(((int)x_1)^((int)x_2));
}

void free_ffann(struct ffann **nn)
{
    for( int index = (*nn)->number_of_layers;
        index > 0;
        index--)
    {
        for( int index2 = (*nn)->layers[index-1]->number_of_nodes;
            index2 > 0;
            index2--)
        {
           free((*nn)->layers[index-1]->layer[index2-1]->inneighborhood);
           free((*nn)->layers[index-1]->layer[index2-1]);
        }
        free((*nn)->layers[index-1]->layer);
        free((*nn)->layers[index-1]);
    }
    free((*nn)->layers);
    free((*nn));
}

void free_sample_set(double **set, int size)
{
    for(int index = 0;
        index < size;
        index++)
    {
        free(set[index]);
    }
    free(set);
}

double forward_propagation_evaluation(struct ffann **nn, double *input_vector)
{
    double *activation_vector;
    int node = 0;
    activation_vector = input_vector;
    //set activation values for input layer
    for(node = 0;
        node < (*nn)->layers[0]->number_of_nodes;
        node++)
    {
        (*nn)->layers[0]->layer[node]->forward_activation_value = 
            input_vector[node];
    }

    for( int layer = 1; 
        layer < (*nn)->number_of_layers;
        layer++)
    {
        for(node = 0;
            node < (*nn)->layers[layer]->number_of_nodes;
            node++)
        {
            (*nn)->layers[layer]->layer[node]->forward_activation_value =
                logistic_function( node_dot_product(
                    (*nn)->layers[layer]->layer[node],
                    activation_vector,
                    (*nn)->layers[layer-1]->number_of_nodes));
        }
        if(layer > 1)
            free(activation_vector);
        if(layer+1 < (*nn)->number_of_layers)
            construct_activation_vector(nn, &activation_vector, layer);
    }
//print errors to stdout
/*
    fprintf(stdout, "Input: (%d,%d) Expected: %d Actual: %f Error %f\n",
        (int)input_vector[0], 
        (int)input_vector[1], 
        (int)input_vector[2], 
        (*nn)->layers[(*nn)->number_of_layers-1]->layer[0]->forward_activation_value, 
        input_vector[2] -
        (*nn)->layers[(*nn)->number_of_layers-1]->layer[0]->forward_activation_value);
*/
    double value = (input_vector[2] -
        (*nn)->layers[(*nn)->number_of_layers-1]->layer[0]->forward_activation_value);
    if(value < 0.0)
        value *= -1.0;
    return value;
}
